import 'reflect-metadata'
import { IClubService } from './components/club-service/types'
import { container } from './di'
import { CONTAINER_TYPES } from './di/utils'
import { ClubService } from './components/club-service'

const clubService = container.get<IClubService>(CONTAINER_TYPES.ClubService) as ClubService

clubService.sendInvitationToAllVisitors()
