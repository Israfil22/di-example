import getDecorators from 'inversify-inject-decorators'
import {container} from './container'

export const CONTAINER_TYPES = {
  'UserService': Symbol.for('UserService'),
  'ClubService': Symbol.for('ClubService'),
  'InvitationService': Symbol.for('InvitationService')
}

export const {lazyInject, lazyInjectNamed, lazyInjectTagged, lazyMultiInject} = getDecorators(container)
