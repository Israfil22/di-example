import {container} from './container'
import { CONTAINER_TYPES } from './utils'
import { UserService } from '../components/user-service'
import { ClubService } from '../components/club-service'
import { InvitationService } from '../components/invitation-service'
import { IUserService } from '../components/user-service/types'
import { IClubService } from '../components/club-service/types'
import { IInvitationService } from '../components/invitation-service/types'

container.bind<IUserService>(CONTAINER_TYPES.UserService).to(UserService)
container.bind<IClubService>(CONTAINER_TYPES.ClubService).to(ClubService)
container.bind<IInvitationService>(CONTAINER_TYPES.InvitationService).to(InvitationService)

export {container}
