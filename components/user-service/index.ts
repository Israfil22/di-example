import { injectable } from 'inversify'
import { lazyInject, CONTAINER_TYPES } from '../../di/utils'
import { IClubService } from '../club-service/types'
import { IUserService } from './types'

@injectable()
export class UserService implements IUserService {
  @lazyInject(CONTAINER_TYPES.ClubService) private club: IClubService
  constructor() {}

  private visitorsStore = [
    {
      club: 'Goo Laguna',
      id: '123456789'
    },
    {
      club: 'Goo Laguna',
      id: '987654321'
    },
    {
      club: 'Gay Party',
      id: '2222222222'
    }
  ]
  get visitors(): Array<string> {
    return this.visitorsStore.filter(v => v.club === this.club.clubName).map(v => v.id)
  }
}
