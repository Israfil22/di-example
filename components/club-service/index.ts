import { injectable } from 'inversify'
import { lazyInject, CONTAINER_TYPES} from '../../di/utils'
import { IClubService } from './types'
import { IUserService } from '../user-service/types'
import { IInvitationService } from '../invitation-service/types'

@injectable()
export class ClubService implements IClubService {
  @lazyInject(CONTAINER_TYPES.UserService) private userService: IUserService
  @lazyInject(CONTAINER_TYPES.InvitationService) private invitationService: IInvitationService
  constructor() {}

  get clubName(): string {
    return 'Goo Laguna'
  }
  sendInvitationToAllVisitors(): void {
    this.userService.visitors.forEach(id => this.invitationService.sendOne(id, 'Hello! Cum to us again :)'))
  }
}
