import { injectable } from 'inversify'
import { IInvitationService } from './types'

@injectable()
export class InvitationService implements IInvitationService {
  sendOne(who: string, message: string): boolean {
    console.log(`Sent to: ${who}. Message: ${message}`)
    return true
  }
}
