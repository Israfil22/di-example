export interface IInvitationService {
  sendOne(who: string, message: string): boolean
}
